import uno
import unohelper
from com.sun.star.beans import XIntrospection
from com.sun.star.task import XJobExecutor
import easymacro as app


ID_EXTENSION = 'net.elmau.zaz.inspect'
SERVICE = ('com.sun.star.task.Job',)

URL_API = 'https://api.libreoffice.org/'
URL_TEMPLATE = URL_API + 'docs/idl/ref/{}com_1_1sun_1_1star_1_1{}.html'
PREFIX = '{}_1_1'


_ = app.install_locales(__file__)


# ~ https://api.libreoffice.org/docs/idl/ref/namespacecom_1_1sun_1_1star_1_1beans_1_1PropertyAttribute.html
PROPERTY_ATTRIBUTE = {
    1 : 'Maybe Void',
    16 : 'Read Only',
}


class Controllers(object):
    MARGIN = 25
    TOOLBAR = 75

    def __init__(self, win):
        self.w = win
        self._current = None
        self._objects = []
        self._type_property = ''

    @property
    def current(self):
        if len(self._objects):
            return self._objects[0]
        return
    @current.setter
    def current(self, value):
        self._current = value
        self._objects.append(value)
        if len(self._objects) > 1:
            self.w.cmd_home.enabled = True
            self.w.cmd_back.enabled = True

    def menu_quit_selected(self, event):
        self.w.close()
        return

    def zaz_inspect_opened(self, event):
        self._get_info()
        return

    def zaz_inspect_resized(self, event):
        width = event.Width - self.MARGIN
        height = event.Height - self.MARGIN
        self.w.tab.size = (width, height)
        height -= self.TOOLBAR
        self.w.tab.grid_properties.size = (width, height)
        self.w.tab.grid_methods.size = (width, height)
        self.w.tab.grid_interfaces.size = (width, height)
        self.w.tab.grid_services.size = (width, height)
        self.w.tab.grid_listeners.size = (width, height)
        return

    def grid_properties_data_changed(self, event):
        self.w.tab.grid_properties.update_row_heading()
        return

    def grid_methods_data_changed(self, event):
        self.w.tab.grid_methods.update_row_heading()
        return

    def grid_interfaces_data_changed(self, event):
        self.w.tab.grid_interfaces.update_row_heading()
        return

    def grid_listeners_data_changed(self, event):
        self.w.tab.grid_listeners.update_row_heading()
        return

    def grid_properties_selection_changed(self, event):
        g = self.w.tab.grid_properties
        row = event.SelectedRowIndexes[0]
        value = '{} = {}'.format(g[0, row], g[2, row])
        app.set_clipboard(value)
        return

    def grid_methods_selection_changed(self, event):
        g = self.w.tab.grid_methods
        row = event.SelectedRowIndexes[0]
        value = '{}{}'.format(g[0, row], g[1, row])
        app.set_clipboard(value)
        return

    def grid_interfaces_selection_changed(self, event):
        g = self.w.tab.grid_interfaces
        row = event.SelectedRowIndexes[0]
        app.set_clipboard(g[0, row])
        return

    def grid_services_selection_changed(self, event):
        g = self.w.tab.grid_services
        row = event.SelectedRowIndexes[0]
        app.set_clipboard(g[0, row])
        return

    def grid_listeners_selection_changed(self, event):
        g = self.w.tab.grid_listeners
        row = event.SelectedRowIndexes[0]
        app.set_clipboard(g[0, row])
        return

    def _show_tuple(self, value):
        self.current = value
        data = value
        if not isinstance(value[0], tuple):
            data = tuple(zip(value))

        g = self.w.tab.grid_properties
        g.data = data

        self.w.tab.grid_methods.clear()
        self.w.tab.grid_interfaces.clear()
        self.w.tab.grid_listeners.clear()
        self._set_name()
        return

    def _validate_object(self, name, value):
        if value is None:
            msg = '{} = None'.format(name)
            app.msgbox(msg)
            return

        if isinstance(value, (str, int, float, bool)):
            msg = '{} = {}'.format(name, value)
            app.msgbox(msg)
            return

        if isinstance(value, tuple):
            if value:
                self._show_tuple(value)
            else:
                msg = _('{} is Empty').format(name)
                app.msgbox(msg)
            return

        if isinstance(value, uno.Enum):
            msg = '{} = {}'.format(name, value.value)
            app.msgbox(msg)
            return

        # ~ app.debug(name, type(value))

        self.current = value
        self._get_info()
        return

    def _get_property(self, name):
        value = None
        try:
            if hasattr(self._current, name):
                value = getattr(self._current, name)
                if value or isinstance(value, str):
                    return value
        except Exception as e:
            app.debug(name)
            app.error(e)
            return '-error-'

        try:
            value = self._current.getPropertyValue(name)
            return value
        except Exception as e:
            app.debug('getPropertyValue')
            app.error(e)

        name = 'get{}'.format(name)
        if hasattr(self._current, name):
            try:
                value = getattr(self._current, name)()
                return value
            except Exception as e:
                app.debug(name)
                app.error(e)

        return '-error-'

    def grid_properties_double_click(self, event):
        g = self.w.tab.grid_properties
        name = g[0, g.row]
        self._type_property = g[1, g.row]
        value = self._get_property(name)
        self._validate_object(name, value)
        return

    def _validate_method(self, name, args):
        if args == '()':
            value = getattr(self._current, name)()
            self._validate_object(name, value)
            return

        if name in ('getByIndex', 'getByName'):
            if name == 'getByIndex':
                data = tuple(range(self._current.getCount()))
            else:
                data = self._current.getElementNames()

            if not data:
                self._validate_object(name, data)
                return

            item = dialog_select(data)
            if not item:
                return

            if name == 'getByIndex':
                item = int(item)
            value = getattr(self._current, name)(item)
            self._validate_object(name, value)
            return

        app.debug('Get Arguments')
        return

    def grid_methods_double_click(self, event):
        g = self.w.tab.grid_methods
        name = g[0, g.row]
        args = g[1, g.row]
        self._validate_method(name, args)
        return

    def cmd_home_action(self, event):
        value = self._objects[0]
        self._objects = []
        self.current = value
        self._get_info()
        self.w.cmd_home.enabled = False
        self.w.cmd_back.enabled = False
        return

    def cmd_back_action(self, event):
        if len(self._objects) == 2:
            self.cmd_home_action(None)
            return

        self._objects.pop()
        value = self._objects[-1]
        self._objects.pop()
        self.current = value
        self._get_info()
        return

    def _set_name(self):
        name = ''
        try:
            name = self._current.ImplementationName
        except:
            pass

        if not name:
            name = self._type_property

        self.w.lbl_name.value = name
        return

    def _get_properties(self):
        g = self.w.tab.grid_properties
        properties = get_properties(self._current)
        if properties is None:
            msg = _('Value is empty')
            app.msgbox(msg)
            return

        self._set_name()
        g.data = properties
        g.sort(0)
        return

    def _get_methods(self):
        g = self.w.tab.grid_methods
        methods, interfaces = get_methods(self._current)
        if methods is None:
            g.clear()
        else:
            g.data = methods
            g.sort(0)

        g = self.w.tab.grid_interfaces
        if interfaces:
            g.data = interfaces
            g.sort(0)
        else:
            g.clear()
        return

    def _get_services(self):
        g = self.w.tab.grid_services
        services = get_services(self._current)
        if services:
            g.data = services
            g.sort(0)
        else:
            g.clear()
        return

    def _get_listeners(self):
        g = self.w.tab.grid_listeners
        listeners = get_listeners(self._current)
        if listeners is None:
            g.clear()
        else:
            g.data = listeners
            g.sort(0)
        return

    def _get_info(self):
        self._get_properties()
        self._get_methods()
        self._get_services()
        self._get_listeners()
        return

    def tab_activated(self, id):
        value = False
        if id > 1:
            value = True
        self.w.cmd_api.enabled = value
        return

    def _get_anchor_md5(self, value):
        """ Generate md5 value in hex from passed value. """
        import hashlib
        h = hashlib.md5()
        h.update(value.encode('utf-8'))
        return 'a' + h.hexdigest()

    def cmd_api_action(self, event):
        id = self.w.tab.active
        grids = {
            2: self.w.tab.grid_methods,
            3: self.w.tab.grid_interfaces,
            4: self.w.tab.grid_services,
            5: self.w.tab.grid_listeners,
        }
        ns = {4: 'service'}

        g = grids[id]
        if g.row < 0:
            msg = _('Select some row')
            app.warning(msg)
            return

        if id == 2:
            value = g[0, g.row]
            parts = g[3, g.row].split('.')
        else:
            parts = g.value.split('.')

        tmp = ''.join([PREFIX.format(p) for p in parts[3:-1]]) + parts[-1]
        url_api = URL_TEMPLATE.format(ns.get(id, 'interface'), tmp)
        app.open_file(url_api)
        return


def dialog_select(data):

    class ControllersDialog(object):

        def __init__(self, dlg):
            self.d = dlg

        def lst_items_double_click(self, event):
            self.d.close(1)
            return

        def cmd_ok_action(self, event):
            if not self.d.lst_items.value:
                msg = _('Select one option')
                app.warning(msg)
                return

            self.d.close(1)
            return

    height = 230
    if len(data) < 20:
        height = len(data) * 10 + 30

    args = {
        'Title': _('Select item'),
        'Width': 150,
        'Height': height,
    }
    dlg = app.create_dialog(args)
    dlg.events = ControllersDialog(dlg)

    args = {
        'Type': 'Listbox',
        'Name': 'lst_items',
        'Width': 140,
        'Height': dlg.height - 30,
        'X': 5,
        'Y': 5,
    }
    dlg.add_control(args)
    dlg.lst_items.data = data

    args = {
        'Type': 'button',
        'Name': 'cmd_ok',
        'Label': _('OK'),
        'Width': 40,
        'Height': 15,
    }
    dlg.add_control(args)
    dlg.center(dlg.cmd_ok, y=-5)

    if dlg.open():
        return dlg.lst_items.value

    return ''


class ZAZInspect(unohelper.Base, XJobExecutor, XIntrospection):

    def __init__(self, ctx):
        self.ctx = ctx
        self.win = None

    def inspect(self, obj):
        if hasattr(obj, 'obj'):
            obj = obj.obj
        self._inspect(obj)
        return

    def trigger(self, args):
        obj = app.get_selection()
        if hasattr(obj, 'obj'):
            obj = obj.obj
        self._inspect(obj)
        return

    def _inspect(self, objuno):
        self._ui()
        self.win.events.current = objuno
        self.win.open()
        return

    def _ui(self):
        args = dict(
            Title = _('ZAZ Inspect'),
            Width = 500,
            Height = 300,
        )

        menu_file = (
            {'label': _('Output')},
            {'label': '-'},
            {'label': _('Quit')},
        )
        menus = (
            {'label': _('~File'), 'submenu': menu_file},
        )

        win = app.create_window(args)
        win.id_extension = ID_EXTENSION
        win.events = Controllers(win)
        win.add_menu(menus)

        args = {
            'Type': 'Button',
            'Name': 'cmd_home',
            'Width': 18,
            'Height': 18,
            'ImageURL': 'home.png',
            'FocusOnClick': False,
            'Enabled': False,
            'X': 5,
        }
        win.add_control(args)

        args = {
            'Type': 'Button',
            'Name': 'cmd_back',
            'Width': 18,
            'Height': 18,
            'ImageURL': 'back.png',
            'FocusOnClick': False,
            'Enabled': False,
        }
        win.add_control(args)
        win.cmd_back.move(win.cmd_home, 2, 0)

        args = {
            'Type': 'Label',
            'Name': 'lbl_name',
            'Label': '',
            'Width': 150,
            'Height': 18,
            'Border': 1,
            'Align': 1,
            'VerticalAlign': 1,
        }
        win.add_control(args)
        win.lbl_name.move(win.cmd_back, 2, 0)

        args = {
            'Type': 'Button',
            'Name': 'cmd_api',
            'Width': 18,
            'Height': 18,
            'ImageURL': 'api.png',
            'FocusOnClick': False,
            'Enabled': True,
            'Enabled': False,
        }
        win.add_control(args)
        win.cmd_api.move(win.lbl_name, 2, 0)


        args = {
            'Type': 'Tab',
            'Name': 'tab',
            'X': 5,
            'Y': 20,
            'Width': 200,
            'Sheets': (
                _('Properties'),
                _('Methods'),
                _('Interfaces'),
                _('Services'),
                _('Listeners'),
            ),
        }
        win.add_control(args)

        columns = (
            {'Title': _('Name')},
            {'Title': _('Type')},
            {'Title': _('Value')},
            {'Title': _('Attributes')},
        )
        args = {
            'Type': 'Grid',
            'Name': 'grid_properties',
            'X': 0,
            'Y': 0,
            'ShowRowHeader': True,
            'Columns': columns,
            'RowHeaderWidth': 15,
        }
        win.tab.add_control(1, args)

        columns = (
            {'Title': _('Name')},
            {'Title': _('Arguments')},
            {'Title': _('Return Type')},
            {'Title': _('Class')},
        )
        args = {
            'Type': 'Grid',
            'Name': 'grid_methods',
            'X': 0,
            'Y': 0,
            'ShowRowHeader': True,
            'Columns': columns,
            'RowHeaderWidth': 15,
        }
        win.tab.add_control(2, args)

        columns = (
            {'Title': _('Name')},
        )
        args = {
            'Type': 'Grid',
            'Name': 'grid_interfaces',
            'X': 0,
            'Y': 0,
            'ShowRowHeader': True,
            'Columns': columns,
            'RowHeaderWidth': 15,
        }
        win.tab.add_control(3, args)

        columns = (
            {'Title': _('Name')},
        )
        args = {
            'Type': 'Grid',
            'Name': 'grid_services',
            'X': 0,
            'Y': 0,
            'ShowRowHeader': True,
            'Columns': columns,
            'RowHeaderWidth': 15,
        }
        win.tab.add_control(4, args)

        columns = (
            {'Title': _('Name')},
        )
        args = {
            'Type': 'Grid',
            'Name': 'grid_listeners',
            'X': 0,
            'Y': 0,
            'ShowRowHeader': True,
            'Columns': columns,
            'RowHeaderWidth': 15,
        }
        win.tab.add_control(5, args)

        self.win = win
        return


def _get_string_value(obj, p):
    type_class = p.Type.typeClass.value
    NAMES = {
        'INTERFACE': '-Interface-',
        'SEQUENCE': '-Sequence-',
        'STRUCT': '-Struct-',
    }
    if type_class in NAMES:
        return NAMES[type_class]

    try:
        value = getattr(obj, p.Name)
    except Exception as e:
        app.debug(p.Name)
        app.error(e)
        return '-error-'

    if isinstance(value, (str, int, float, bool)):
        return str(value)

    if type_class == 'ENUM' and value:
        return str(value.value)

    if type_class == 'TYPE':
        return value.typeName

    if value is None:
        return '-void-'

    app.debug('Property', p.Name, value)

    return ''


def get_properties(obj):
    intro = app.create_instance('com.sun.star.beans.Introspection')
    result = intro.inspect(obj)

    if result is None:
        return

    properties = result.getProperties(-1)
    data = []
    for p in properties:
        name = p.Name
        tipo = p.Type.typeName
        value = _get_string_value(obj, p)
        attr = ''
        if p.Attributes:
            pa = PROPERTY_ATTRIBUTE
            a = p.Attributes
            attr = ', '.join([pa.get(k, '') for k in pa.keys() if a & k])
            if not attr:
                app.error(p.Attributes)
        data.append((name, tipo, value, attr))

    return tuple(data)


def get_methods(obj):
    intro = app.create_instance('com.sun.star.beans.Introspection')
    result = intro.inspect(obj)

    if result is None:
        return

    methods = result.getMethods(-1)
    data = []
    interfaces = set()
    for m in methods:
        name = m.Name
        arguments = '()'
        if len(m.ParameterInfos) > 0:
            for p in m.ParameterInfos:
                arguments = '( {} )'.format(', '.join(
                    [' '.join((
                        '[{}]'.format(p.aMode.value.lower()),
                        p.aName,
                        p.aType.Name)) for p in m.ParameterInfos]
                ))
        return_type = m.ReturnType.Name
        name_class = m.DeclaringClass.Name
        interfaces.add(name_class)
        data.append((name, arguments, return_type, name_class))

    return tuple(data), tuple(zip(interfaces))


def get_services(obj):
    try:
        data = [str(s) for s in obj.getSupportedServiceNames()]
        data = tuple(zip(data))
    except:
        data = ()
    return data


def get_listeners(obj):
    intro = app.create_instance('com.sun.star.beans.Introspection')
    result = intro.inspect(obj)

    if result is None:
        return

    data = [l.typeName for l in result.getSupportedListeners()]
    return tuple(zip(data))


g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(ZAZInspect, ID_EXTENSION, SERVICE)
